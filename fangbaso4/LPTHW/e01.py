# py3 print 需要加（）
# 书中练习
print ("Hello World!")
print ("Hello Again")
print ("I like typing this.")
print ("This is fun.")
print ('Yay! Printing.')
print ("I'd much rather you 'not'.")
print ('I "said" do not touch this.')

# print练习：
## 不同类型输出
print("string") #输出字符串
print(100)  #输出int
str = 'Hello every one~'
print(str)  #print out val
## 格式化输出
print('My name is %s,today learned %s %.2f hours'%('fangbaso4','python',0.5))

## 
print ('code 中分行 '
,'但终端不换行')
print('code内容不换行\n但是输出内容换行\n哈哈哈')
print ("末尾不换行",end='')
print('look, no line break')